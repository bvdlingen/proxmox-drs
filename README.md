## Installation:

This script is written to use Python 3.
Install pip3 to installed the required Python packages

> apt-get install python3-pip

> pip3 install proxmoxer

> pip3 install config


## Configuration:

The config.py file contains all parameters that need to be set to use the script.

PROXMOX_HOST: *This is the login details needed to access the Proxmox API which is used to create the VMs.*

drs_aggression: *1-10, drs_aggression is used to loop n times per run of the script. i.e. can migration up to n VMs per script run.*

top_vms: *1-n, top_vms is used to select the n-th top VM to migrate to the destination node.*

min_vms: *1-n, min_vms is the minimum number of VMs to leave on a node even if the node has high CPU.*

maint_path: *maint_path is the path to where the .maintenance files will be created. The .maintenance file triggers a migration of all VMs of that host. The file is in the format of 'hostname'.maintenance.*


## Usage:

The Proxmox Dynamic Resource System script load balances Proxmox nodes in a cluster. The load balancing is based on CPU usage across the cluster. A band of CPU usage is calculated and any node which falls outside of this band will have VMs migrated on or off.

The script is designed to run as a Cron job on a regular interval, the suggested interval is 5 minutes. This gives the script enough time to migrate the drs_aggression number of VMs between nodes before the script is run again.


> python3 proxmox-drs.py



